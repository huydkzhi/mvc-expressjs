var workforce = require('workforce');
 
var manager = workforce('server.js')
  .set('workers', 4)
  .set('title', 'awesome app')
  .set('restart threshold', '10s')
  .set('exit timeout', '5s');
 
// manager.configure('development', function(){
//   manager.use(workforce.watch(['./lib']));
// });
 
// manager.configure('production', function(){
//   manager.set('working directory', '/');
// });
 
manager.listen(3000);