var events = require('events');
var async = require('async');
var emitter = module.exports.emitter = new events.EventEmitter();

var util = require('util');
var Sequelize = require('sequelize');
var sequelize = undefined;
var User = undefined;
 
module.exports.connect = function(params, callback) {
    sequelize = new Sequelize(params.dbname,
                              params.username,
                              params.password,
                              params.params);
  
    Messages = sequelize.define('Messages', {
        idTo: { type: Sequelize.INTEGER, unique: false},
        idFrom: { type: Sequelize.INTEGER, unique: false},
        message: { type: Sequelize.STRING, unique: false}
    });
    Messages.sync();
}

module.exports.sendMessage = function(id, from, message, callback) {
    Messages.create({
        idTo: id,
        idFrom: from,
        message: message
    }).then(function(user) {
        emitter.emit('newmessage', id);
        callback();
    }).error(function(err) {
        callback(err);
    });
}
 
module.exports.getMessages = function(id, callback) {
    Messages.findAll({
        where: { idTo: id}
    }).then(function(messages) {
        if(messages) {
            var messageList = [];
            async.eachSeries(messages, 
               function(msg, done) {
                   module.exports.findById(msg.idFrom,
                       function(err, userFrom) {
                           messageList.push({
                               idTo: msg.idTo,
                               idFrom: msg.idFrom,
                               fromName: userFrom.username,
                               message: msg.message
                            });
                            done();
                        });
                    },
                    function(err) {
                        if(err) 
                            callback(err);
                        else
                            callback(null, messageList);
                        }
                    );
        } else {
            callback();
        }
    });
}
 
module.exports.delMessage = function(id, from, message, callback) {
    Messages.find({
        where: { idTo: id, idFrom: from, message: message}
    }).then(function(msg) {
        if(msg) {
            msg.destroy().then(function() {
                emitter.emit('delmessage');
                callback();
            }).error(function(err) {
                callback(err);
            });
        } else
            callback();
    });
}
