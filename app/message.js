var message       = require('./models/message');
var user          = require('./models/user.js');
message.connect({
    dbname: "notes",
    username: "root",
    password: "Topica@123##",
    params: {
        host: "42.112.28.129",
        dialect: "mysql"
    }
},
function(err) {
    if(err)
        throw err;
});


module.exports = function(app,server) {

    app.use('/sendmessage', function(req, res) {
        users.allUsers(function(err, userList) {
           res.render('sendmessage', {
               title: "Send a message",
               user: req.user,
               users: userList,
               message: req.flash('error')
           });
        });
    });

    app.post('/dosendmessage', users.ensureAuthenticated, users.doSendMessage)

	var io = require('socket.io').listen(server);
// normal routes ===========================================================
	
        io.sockets.on('connection', function(socket) {
            socket.on('getmessages', function(id, fn) {
            usersModels.getMessages(id, function(err, messages) {
                if(err) {
                    util.log('getmessages ERROR ' + err);
                } else
                    fn(messages);
            });
        });
      
        var broadcastNewMessage = function(id) {
            socket.emit('newmessage', id);
        }
        usersModels.emitter.on('newmessage', broadcastNewMessage);
      
        var broadcastDelMessage = function() {
            socket.emit('delmessage');
        }
        usersModels.emitter.on('delmessage', broadcastDelMessage);
      
        socket.on('disconnect', function() {
            usersModels.emitter.removeListener('newmessage', broadcastNewMessage);
            usersModels.emitter.removeListener('delmessage', broadcastDelMessage);
        });
      
        socket.on('dodelmessage', function(id, from, message, fn) {
            // do nothing
        });
    });
}

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}
