var workforce = require('workforce');
var manager = workforce('./server.js');
manager.set('workers', 2);
manager.set('title', 'Notes');
manager.set('restart threshold', '10s');
manager.set('exit timeout', '5s');
manager.listen(process.env.PORT || 8080);