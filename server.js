// server.js
var http = require('http');
var compression = require('compression');
// set up ======================================================================
// get all the tools we need
var express  = require('express');
var app      = express();
var port     = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');
var path = require('path');
var favicon = require('serve-favicon');
var expressLayouts = require('express-ejs-layouts');

var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');

var configDB = require('./config/database.js');
var server = require('http').createServer(app);


// configuration ===============================================================
mongoose.connect(configDB.url); // connect to our database

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
// app.use(favicon(path.join(__dirname, 'public', 'favicon2.png')));
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
// app.use(compression());
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set("layout extractScripts", true);
app.use(expressLayouts);
app.set('layout', 'layout/layout');
app.set("layout extractScripts", true);
app.locals.base = "http://localhost:8000/";


// required for passport
app.use(session({
    secret: 'iloveyou', // session secret
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// launch ======================================================================

// notes =======================================================================
// console.log(server.listen(process.env.PORT || 8000));
require('./app/notes.js')(app,server);
server.listen(process.env.PORT || 8000); 
// app.listen(port);
console.log('The magic happens on port ' + port);

module.exports = app;