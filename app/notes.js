var notes       = require('./models/notes');

notes.connect({
    dbname: "notes",
    username: "root",
    password: "Topica@123##",
    params: {
        host: "42.112.28.129",
        dialect: "mysql"
    }
},
function(err) {
    if(err)
        throw err;
});
var readNote = function(key, res, done) {
    notes.read(key, 
        function(err, data) {
            if(err) {
            res.render('notes/showerror', {
                title: "Could not read note " + key,
                error: err
            });
        done(err);
    } else
        done(null, data);
    });
}

module.exports = function(app,server) {
	var io = require('socket.io').listen(server);
// normal routes ===========================================================
	app.get('/notes', function(req, res) {
		notes.titles(function(err, noteList) {
	        if(err) 
	            res.render('notes/showerror', { title: 'Notes', error: 'Could not read data'});
	        else
	            res.render('notes/index', { title: 'Notes', notes: noteList });
	    }); 
    });

    app.get('/noteadd',isLoggedIn, function(req, res) {
	    res.render('notes/noteedit', {
	        title: "Add a note",
	        docreate: true,
	        notekey: "",
	        note: undefined
	    });
    });

    app.post('/notesave',isLoggedIn, function(req, res) {
       ((req.body.docreate === "create") ? notes.create : notes.update)
	    (req.body.notekey, req.body.title, req.body.body,
	        function(err) {
		        if(err) {
		            res.render('notes/showerror', {
		                title: "Could not update file",
		                error: err
		            });
		        } else {
		            res.redirect('/noteview?key='+req.body.notekey);
		        }
        	});
    });

    app.get('/noteview',isLoggedIn, function(req, res) {
	   if(req.query.key) {
        	readNote(req.query.key, res, function(err, data) {
	            if(!err) {
	                res.render('notes/noteview', {
	                    title: data.title,
	                    notekey: req.query.key,
	                    note: data
	                });
	            }
        	});
	    } else {
	        res.render('notes/showerror', {
	            title: "No key given for Note",
	            error: "Must provide a Key to view a Note"
	        });
	    }
    });

    app.get('/noteedit',isLoggedIn, function(req, res) {
	    if(req.query.key) {
        readNote(req.query.key, res, function(err, data) {
            if(!err) {
	                res.render('notes/noteedit', {
	                    title: data ? ("Edit " + data.title) : "Add a Note",
	                    docreate: false,
	                    notekey: req.query.key,
	                    note: data
	                });
	            }
        	});
	    } else {
	        res.render('notes/showerror', {
	            title: "No key given for Note",
	            error: "Must provide a Key to view a Note"
	        });
	    }
    });

    app.get('/notedestroy',isLoggedIn, function(req, res) {
	    if(req.query.key) {
        	readNote(req.query.key, res, function(err, data) {
	            if(!err) {
	                res.render('notes/notedestroy', {
	                    title: data.title,
	                    notekey: req.query.key,
	                    note: data
	                });
	            }
        	});
	    } else {
	        res.render('notes/showerror', {
	            title: "No key given for Note",
	            error: "Must provide a Key to view a note"
	        });
	    }
    });

    app.post('/notedodestroy',isLoggedIn, function(req, res) {
	   notes.destroy(req.body.notekey, function(err) {
        if(err) {
            res.render('notes/showerror', {
            title: "Could not delete Note " + req.body.notekey,
            error: err
            });
        } else {
            res.redirect('/notes');
        }
    });
    });

    io.sockets.on('connection', function(socket) {
    socket.on('notetitles', function(fn) {        
        notes.titles(function(err, titles) {
            if(err) {
                util.log(err); 
            } else {
                fn(titles);
            }
        });
     });
  
    var broadcastUpdated = function(newnote) {
        socket.emit('noteupdated', newnote);
    }
    notes.emitter.on('noteupdated', broadcastUpdated);
    socket.on('disconnect', function() {
       notes.emitter.removeListener('noteupdated', broadcastUpdated);
    });
 
    var broadcastDeleted = function(notekey) {
       socket.emit('notedeleted', notekey);
    }
    notes.emitter.on('notedeleted', broadcastDeleted);
    socket.on('disconnect', function() {
        notes.emitter.removeListener('notedeleted', broadcastDeleted);
    });
 
});
}

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}
